export class ListPoints {
    constructor() {
        this.list = [];
        this.content = {
            imagem: "",
            titulo: "",
            descricao: "",
        };

        this.selectors();
        this.events();
    }

    selectors() {
        this.form = document.querySelector(".form");
        this.imageInput = document.querySelector(".input-image");
        this.imagePlaceHolder = document.querySelector(".input-image-placeholder");
        this.titleInput = document.querySelector(".input-title");
        this.descriptionInput = document.querySelector(".input-description");
        this.listResult = document.querySelector(".list-result");
        this.formInput = document.querySelector(".form-input-image");
    }

    onFileSelected(event) {
        let selectedFile = event.target.files[0];
        let reader = new FileReader();
        const _this = this;

        this.imagePlaceHolder.title = selectedFile.name;

        reader.onload = function (event) {
            _this.imagePlaceHolder.src = event.target.result;

            _this.content.imagem = event.target.result;

            _this.formInput.classList.add("active");
        };

        reader.readAsDataURL(selectedFile);
    }

    events() {
        this.form.addEventListener("submit", this.addTitleToList.bind(this));
        this.imageInput.addEventListener("change", this.onFileSelected.bind(this));
    }

    addTitleToList(event) {
        event.preventDefault();

        const titleName = event.target["Título"].value;
        const descriptionName = event.target["Descrição"].value;

        if (titleName !== "" && descriptionName !== "" && this.content.imagem !== "") {
            this.content.titulo = titleName;
            this.content.descricao = descriptionName;

            this.list.push(this.content);
            this.renderList();
            this.resetInputs();
        }
    }

    renderList() {
        let contentStructure = "";

        const item = this.list[this.list.length - 1];

        contentStructure += `
            <div class="list-item">
                <img src="${item.imagem}" />
                <div class="list-item-info">
                    <h3>${item.titulo}</h3>
                    <span>${item.descricao}</span>
                </div>
            </div>`;

        this.listResult.innerHTML += contentStructure;
    }

    resetInputs() {
        this.titleInput.value = "";
        this.descriptionInput.value = "";
        this.formInput.classList.remove("active");
    }
}
